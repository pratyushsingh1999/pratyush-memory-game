const  throttler  =  function (func, delay) {
    let timerId;
    return function () {
        if (timerId) {
            return
        }
        timerId  =  setTimeout(function () {
            func()
            timerId  =  undefined;
        }, delay)
    }
}
export default throttler;
