const debouncer = function (fun , deb) {
    let timer;
    return function () {
        clearTimeout(timer);
        timer = setTimeout(fun , deb);
    }
}

export default debouncer;
