import React from 'react';
import { Link,Redirect } from 'react-router-dom'
import './Signup.css';
import SignupSchema from "../../form-schemas/signup-schema";
import {Field, Form, Formik} from "formik";
import Axios from 'axios';

class Signup extends React.Component {
    state = {
        loading: false,
        redirect: false
    }
    render() {
        if (this.state.redirect) {
            return <Redirect to="/login"></Redirect>
        }
        return (
            <div className="main-login-wrapper">
                <Formik
                    initialValues={{
                        name: '',
                        email: '',
                        password: '',
                        passwordConfirm: ''
                    }}
                    validationSchema={SignupSchema}
                    onSubmit={values => {
                        this.setState({
                            loading: true
                        }, () => {
                            Axios.post('api/signup', {
                                name: values.name,
                                email: values.email,
                                password: values.password
                            }, {
                                headers: {
                                    'Content-Type': 'application/json;charset=UTF-8',
                                    "Access-Control-Allow-Origin": "*",
                                    "Accept": "application/json"
                                },
                                referrerPolicy: 'no-referrer',
                            }).then((data) => {
                                this.setState({
                                    redirect: true,
                                    loading: false
                                })
                            }).catch((err) => {
                                this.setState({
                                    loading: false
                                })
                            })
                        })
                    }}
                >
                    {({ errors, touched ,isValid,dirty}) => (
                        <Form className="login-wrapper">
                            <p>Signup</p>
                            <Field name="name" type="text" placeholder="Enter name"/>
                            {errors.name && touched.name ? <div className="auth-error">{errors.name}</div> : null}
                            <Field name="email" type="email" placeholder="Enter email"/>
                            {errors.email && touched.email ? <div className="auth-error">{errors.email}</div> : null}
                            <Field name="password" type="password" placeholder="Enter Password"/>
                            {errors.password && touched.password ? (<div className="auth-error">{errors.password}</div>) : null}
                            <Field name="passwordConfirm" type="password" placeholder="Confirm Password"/>
                            {errors.passwordConfirm && touched.passwordConfirm ? (<div className="auth-error">{errors.passwordConfirm}</div>) : null}
                            <Link className="auth-switch" to="/login">Already have an account! Login?</Link>
                            {
                                this.state.loading ? <div className="loader" style={{color: "#253237" , margin: '10px 20px 10px' , fontSize: '20px' , alignSelf: 'flex-end'}}></div> :
                                    <button type="submit" className={!isValid || !dirty ? 'disabled': ''}>Submit</button>
                            }
                        </Form>
                    )}
                </Formik>
            </div>
        )
    }
}
export default Signup;
