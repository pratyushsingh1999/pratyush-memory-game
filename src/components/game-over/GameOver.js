import React from 'react';
import { Link } from "react-router-dom";
import './GameOver.css';
import SendMail from "../send-mail/SendMail";
import {connect} from "react-redux";
import {updateGameScore} from "../../store/actions/gameActions";

class GameOver extends React.Component {
    state = {
        playAgain: false,
        viewGlobalScores: false,
        bestEasy: localStorage.getItem('bestEasy') || 'Not Played Yet',
        bestMedium: localStorage.getItem('bestMedium') || 'Not Played Yet',
        bestHard: localStorage.getItem('bestHard') || 'Not Played Yet',
        showSendCertificate: false
    }
    componentDidMount() {
        switch (this.props.difficulty) {
            case 'easy':
                if (localStorage.getItem('bestEasy') === null) {
                    localStorage.setItem('bestEasy' , this.props.score);
                    this.setState({
                        bestEasy: this.props.score
                    })
                }else {
                    if (localStorage.getItem('bestEasy') > this.props.score) {
                        localStorage.setItem('bestEasy' , this.props.score);
                        this.setState({
                            bestEasy: this.props.score,
                            showSendCertificate: true
                        })
                    }
                }
                break;
            case 'medium':
                if (localStorage.getItem('bestMedium') === null) {
                    localStorage.setItem('bestMedium', this.props.score);
                    this.setState({
                        bestMedium: this.props.score
                    })
                }else {
                    if (localStorage.getItem('bestMedium') > this.props.score) {
                        localStorage.setItem('bestMedium' , this.props.score);
                        this.setState({
                            bestMedium: this.props.score,
                            showSendCertificate: true
                        })
                    }
                }
                break;
            case 'hard':
                if (localStorage.getItem('bestHard') === null) {
                    localStorage.setItem('bestHard' , this.props.score);
                    this.setState({
                        bestHard: this.props.score
                    })
                }else {
                    if (localStorage.getItem('bestHard') > this.props.score) {
                        localStorage.setItem('bestHard' , this.props.score);
                        this.setState({
                            bestHard: this.props.score,
                            showSendCertificate: true
                        })
                    }
                }
                break;
            default:
        }
    }
    render() {
        let sendEmail = '';
        if (this.state.showSendCertificate) {
            sendEmail = <SendMail email={this.props.email} score={this.props.score}></SendMail>;
        }
        return (
            <div className="game-over-wrapper">
                <p>Game over, your score is {this.props.score}</p>
                {sendEmail}
                <table>
                    <thead>
                    <tr>
                        <th>Game Mode</th>
                        <th>Best Score</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Easy</td>
                        <td>{this.state.bestEasy}</td>
                    </tr>
                    <tr>
                        <td>Medium</td>
                        <td>{this.state.bestMedium}</td>
                    </tr>
                    <tr>
                        <td>Hard</td>
                        <td>{this.state.bestHard}</td>
                    </tr>
                    </tbody>
                </table>
                <Link to="/global-scores">
                    <button>View Global Scores</button>
                </Link>
                <Link to="/start">
                    <button onClick={() => this.props.updateGameScore(0)}>Play Again</button>
                </Link>
            </div>
        )
    }
}
const mapStateToProps = state => ({
    email: state.user.email,
    gameMode: state.game.gameMode,
    difficulty: state.game.difficulty,
    score: state.game.score
})

export default connect(mapStateToProps, {updateGameScore})(GameOver);
