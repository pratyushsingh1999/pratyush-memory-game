import React from 'react';
import './SendMail.css';
import Axios from "axios";

class SendMail extends React.Component {
    state = {
        email: this.props.email,
        score: this.props.score,
        token: localStorage.getItem('auth_token'),
        emailSent: false,
        sendingEmail: false
    }
    sendCertificate = () => {
        this.setState({
            sendingEmail: true
        }, () => {
            Axios.post('api/sendMail/highestScore', {
                email: this.state.email,
                count: this.state.score
            }, {
                headers: {
                    'Content-Type': 'application/json;charset=UTF-8',
                    "Access-Control-Allow-Origin": "*",
                    "Accept": "application/json",
                    "authorization": "bearer " + this.state.token
                },
                referrerPolicy: 'no-referrer',
            }).then((res) => {
                this.setState({
                    sendingEmail: false,
                    emailSent: true
                })
            }).catch(() => {
                this.setState({
                    sendingEmail: false,
                    emailSent: false
                })
            })
        })
    }

    render() {
        if (this.state.emailSent) {
            return <div></div>
        } else {
            return (
                <div className="send-mail-wrapper">
                    {this.state.sendingEmail ?
                        <div className="loader"></div> :
                        <div className="send-mail-wrapper">
                            <p>Congratulations this is your highest score</p>
                            <p>Do you want us to send you a certificate??</p>
                            <button onClick={this.sendCertificate}>Send Certificate</button>
                        </div>
                    }
                </div>
            )
        }
    }
}

export default SendMail;
