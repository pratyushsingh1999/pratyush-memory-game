import React from 'react';
import {Link, Redirect} from 'react-router-dom'
import ColorTiles from "../tiles/colour-tiles/ColorTiles";
import ImageTiles from "../tiles/image-tiles/ImageTiles";
import './GameBoard.css'
import gif1 from '../../assets/gifs/1.gif';
import gif2 from '../../assets/gifs/2.gif';
import gif3 from '../../assets/gifs/3.gif';
import gif4 from '../../assets/gifs/4.gif';
import gif5 from '../../assets/gifs/5.gif';
import gif6 from '../../assets/gifs/6.gif';
import gif7 from '../../assets/gifs/7.gif';
import gif8 from '../../assets/gifs/8.gif';
import gif9 from '../../assets/gifs/9.gif';
import gif10 from '../../assets/gifs/10.gif';
import gif11 from '../../assets/gifs/11.gif';
import gif12 from '../../assets/gifs/12.gif';
import {connect} from "react-redux";
import { updateGameScore } from "../../store/actions/gameActions";

class GameBoard extends React.Component {
    state = {
        noOfTiles: 0,
        noOfActiveTiles: 0,
        score: 0,
        gameOver: false,
        gameMode: this.props.gameMode,
        tiles: [],
        previousClick: null,
        navigate: false,
        restart: false
    }
    // Helper Functions
    shuffle = (array) => {
        let counter = array.length;
        while (counter > 0) {
            let index = Math.floor(Math.random() * counter);
            counter--;
            let temp = array[counter];
            array[counter] = array[index];
            array[index] = temp;
        }
        return array;
    }
    getRandomColour = () => {
        return '#' + Math.random().toString(16).substr(-6);
    }
    allGifsArr = [
        gif1,
        gif2,
        gif3,
        gif4,
        gif5,
        gif6,
        gif7,
        gif8,
        gif9,
        gif10,
        gif11,
        gif12
    ];
    gifsUsed = [];
    getRandomGif = () => {
        let randomNumber = Math.floor(Math.random()*11);
        while (this.gifsUsed.includes(randomNumber)) {
            randomNumber = Math.floor(Math.random()*11);
        }
        this.gifsUsed.push(randomNumber);
        return this.allGifsArr[randomNumber];
    }
    increaseScore = () => {
        this.props.updateGameScore(this.props.score + 1);
        this.setState({
            score: this.state.score + 1
        })
    }
    // Click Handlers
    handleClickColor = (index , color) => {
        const currentClick = { index, color }
        const previousClick = this.state.previousClick;
        if (previousClick === null) {
            this.increaseScore();
            this.setState({
                previousClick: currentClick,
                tiles: this.state.tiles.map((tile , index) => {
                    let tempTile = {...tile};
                    if (index === currentClick.index) {
                        tempTile = {...tile, clicked: true }
                    }
                    return tempTile;
                })
            })
            return;
        }
        if (previousClick.color === currentClick.color) {
            document.getElementById('board').style.setProperty('pointer-events','none');
            this.increaseScore();
            this.setState({
                noOfActiveTiles: this.state.noOfActiveTiles - 2,
                previousClick: null,
                tiles: this.state.tiles.map((tile , index) => {
                    let tempTile = { ...tile }
                    if (index === currentClick.index || index === previousClick.index) {
                        tempTile = {
                            ...tile,
                            clicked: true
                        }
                        return tempTile;
                    }
                    return tempTile;
                })
            })
            setTimeout(() => {
                document.getElementById('board').style.setProperty('pointer-events','auto');
                if (this.state.noOfActiveTiles === 0) {
                    this.setState({
                        navigate: true
                    })
                }
                this.setState({
                    tiles: this.state.tiles.map((tile , index) => {
                        let tempTile = { ...tile }
                        if (index === currentClick.index || index === previousClick.index) {
                            tempTile = {
                                ...tile,
                                matched: true
                            }
                            return tempTile;
                        }
                        return tempTile;
                    })
                })
            }, 1000)
            return;
        }
        if (previousClick.color !== currentClick.color) {
            document.getElementById('board').style.setProperty('pointer-events','none');
            this.setState({
                previousClick: currentClick,
                tiles: this.state.tiles.map((tile , index) => {
                    let tempTile = {...tile};
                    if (index === currentClick.index) {
                        tempTile = {...tile, clicked: true }
                    }
                    return tempTile;
                })
            })
            setTimeout(() => {
                document.getElementById('board').style.setProperty('pointer-events','auto');
                this.setState({
                    previousClick: null,
                    tiles: this.state.tiles.map((tile , index) => {
                        let tempTile = {...tile};
                        if (index === currentClick.index || index === previousClick.index) {
                            tempTile = {...tile , clicked: false }
                        }
                        return tempTile;
                    })
                })
            },1000)
            this.increaseScore();
            return;
        }
    }
    handleClickImage = (index , src) => {
        const currentClick = { index, src }
        const previousClick = this.state.previousClick;
        if (previousClick === null) {
            this.increaseScore();
            this.setState({
                previousClick: currentClick,
                tiles: this.state.tiles.map((tile , index) => {
                    let tempTile = {...tile};
                    if (index === currentClick.index) {
                        tempTile = {...tile, clicked: true }
                    }
                    return tempTile;
                })
            })
            return;
        }
        if (previousClick.src === currentClick.src) {
            document.getElementById('board').style.setProperty('pointer-events','none');
            this.increaseScore();
            this.setState({
                noOfActiveTiles: this.state.noOfActiveTiles - 2,
                previousClick: null,
                tiles: this.state.tiles.map((tile , index) => {
                    let tempTile = { ...tile }
                    if (index === currentClick.index || index === previousClick.index) {
                        tempTile = {
                            ...tile,
                            clicked: true
                        }
                        return tempTile;
                    }
                    return tempTile;
                })
            })
            setTimeout(() => {
                document.getElementById('board').style.setProperty('pointer-events','auto');
                if (this.state.noOfActiveTiles === 0) {
                    this.setState({
                        navigate: true
                    })
                }
                this.setState({
                    tiles: this.state.tiles.map((tile , index) => {
                        let tempTile = { ...tile }
                        if (index === currentClick.index || index === previousClick.index) {
                            tempTile = {
                                ...tile,
                                matched: true
                            }
                            return tempTile;
                        }
                        return tempTile;
                    })
                })
            }, 1000)
            console.log(this.state);
            return;
        }
        if (previousClick.src !== currentClick.src) {
            document.getElementById('board').style.setProperty('pointer-events','none');
            this.setState({
                previousClick: currentClick,
                tiles: this.state.tiles.map((tile , index) => {
                    let tempTile = {...tile};
                    if (index === currentClick.index) {
                        tempTile = {...tile, clicked: true }
                    }
                    return tempTile;
                })
            })
            setTimeout(() => {
                document.getElementById('board').style.setProperty('pointer-events','auto');
                this.setState({
                    previousClick: null,
                    tiles: this.state.tiles.map((tile , index) => {
                        let tempTile = {...tile};
                        if (index === currentClick.index || index === previousClick.index) {
                            tempTile = {...tile , clicked: false }
                        }
                        return tempTile;
                    })
                })
            },1000)
            this.increaseScore();
            return;
        }
    }
    loadTiles = () => {
        let tilesArr = []
        switch (this.props.gameMode) {
            case "Color":
                for (let index = 0; index < this.state.noOfTiles/2; index++) {
                    let colourAssigned = this.getRandomColour();
                    let tilesObj = {
                        color: colourAssigned,
                        matched: false,
                        clicked: false
                    }
                    tilesArr.push(tilesObj)
                    tilesArr.push(tilesObj)
                }
                tilesArr = this.shuffle(tilesArr);
                break;
            case "Gif":
                for (let index = 0; index < this.state.noOfTiles/2; index++) {
                    let srcAssigned = this.getRandomGif();
                    let tilesObj = {
                        src: srcAssigned,
                        matched: false,
                        clicked: false
                    }
                    tilesArr.push(tilesObj)
                    tilesArr.push(tilesObj)
                }
                tilesArr = this.shuffle(tilesArr);
                break
            default:
                break
        }
        this.setState({
            tiles: tilesArr
        })
    }
    componentDidMount() {
        switch (this.props.difficulty) {
            case "easy":
                this.setState({
                    noOfTiles: 6,
                    noOfActiveTiles: 6
                }, () => {
                    this.loadTiles()
                })
                break;
            case "medium":
                this.setState({
                    noOfTiles: 8,
                    noOfActiveTiles: 8
                }, () => {
                    this.loadTiles()
                })
                break;
            case "hard":
                this.setState({
                    noOfTiles: 12,
                    noOfActiveTiles: 12
                }, () => {
                     this.loadTiles()
                })
                break;
            default:
        }
    }
    toggleRestart = () => {
        this.setState({
            restart: !this.state.restart
        })
    }
    render() {
        let loadSelectedTile = [];
        switch (this.props.gameMode) {
            case "Color":
                loadSelectedTile = this.state.tiles.map((tile , index) => {
                    return <ColorTiles tile={tile}
                                       key={index}
                                       index={index}
                                       handleClick={this.handleClickColor.bind(this)}></ColorTiles>
                })
                break;
            case "Gif":
                loadSelectedTile = this.state.tiles.map((tile , index) => {
                    return <ImageTiles tile={tile}
                                       key={index}
                                       index={index} handleClick={this.handleClickImage}></ImageTiles>
                })
                break;
            default:
                loadSelectedTile = <h2>Some Error please refresh the game</h2>
        }
        if (this.state.navigate) {
            return <Redirect to='/over'/>
        }
        let restartButtons = '';
        if (!this.state.restart) {
            restartButtons = <button onClick={this.toggleRestart} className="normal-button">Restart Game?</button>
        }else {
            restartButtons = <div>
                <Link to="/start">
                    <button onClick={() => this.props.updateGameScore(0)}
                            className="normal-button">
                        Yes
                    </button>
                </Link>
                <button onClick={this.toggleRestart} className="warn-button">No</button>
            </div>
        }
        return (
            <div style={{display: 'flex' , flexDirection: "column", alignItems: "center"}}>
                <div className="wrapper" id="board">
                    {loadSelectedTile}
                </div>
                {restartButtons}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    gameMode: state.game.gameMode,
    difficulty: state.game.difficulty,
    score: state.game.score
})

export default connect(mapStateToProps, {updateGameScore})(GameBoard);
