import React from 'react';
import './Header.css';
import {Redirect} from "react-router-dom";
import {connect} from "react-redux";
import {logoutUser} from "../../store/actions/authActions";

class Header extends React.Component {
    constructor() {
        super();
        this.state = {
            logout: false
        }
    }
    logout = () => {
        localStorage.removeItem('auth_token');
        this.props.logoutUser();
        this.setState({
            logout: true
        })
    }
    render() {
        let user;
        if (this.state.logout) {
            this.setState({
                logout: false,
            })
            return <Redirect to='/login'/>
        }
        if (this.props.loggedIn) {
            user = <div className="user-info-wrapper">
                <p>Welcome {this.props.user.username}</p>
                <button onClick={this.logout}>Logout</button>
            </div>
        }else {
            user = '';
        }
        return (
            <div className="main-wrapper">
                <p>Welcome to Memory Game</p>
                {user}
            </div>
        )
    }
}
const mapStateToProps = state => ({
    loggedIn: state.auth.loggedIn,
    user: state.user
})
export default connect(mapStateToProps, {logoutUser})(Header);
