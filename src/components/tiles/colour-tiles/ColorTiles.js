import React from 'react';
import './ColorTiles.css'

const ColorTiles = (props) => {
    const matched = props.tile.matched;
    const color = props.tile.color;
    const handleClick = props.handleClick;
    const clicked = props.tile.clicked;
    if (matched) {
        return (
            <div>
                <div className="tile-matched">
                    <p>Matched</p>
                </div>
            </div>
        )
    }
    if (clicked) {
        return (
            <div>
                <div className="tile-show"
                     style={{backgroundColor: color, pointerEvents: "none"}}
                     onClick={handleClick.bind(this, props.index, color)}>
                </div>
            </div>
        )
    } else {
        return (
            <div>
                <div className="tile-show"
                     onClick={handleClick.bind(this, props.index, color)}>
                </div>
            </div>
        )
    }
}

export default ColorTiles;
