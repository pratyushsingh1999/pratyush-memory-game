import React from 'react';
import './ImageTiles.css';

const ImageTiles = (props) => {
    const matched = props.tile.matched;
    const src = props.tile.src;
    const handleClick = props.handleClick;
    const clicked = props.tile.clicked;
    if (matched) {
        return (
            <div>
                <div className="tile-matched">
                    <p>Matched</p>
                </div>
            </div>
        )
    }
    if (clicked) {
        return (
            <div>
                <img className="tile-show"
                     src={src} alt="smiley face"
                     style={{pointerEvents: "none"}}
                     onClick={handleClick.bind(this, props.index, src)}>
                </img>
            </div>
        )
    } else {
        return (
            <div>
                <div className="tile-show"
                     onClick={handleClick.bind(this, props.index, src)}>
                </div>
            </div>
        )
    }
}

export default ImageTiles;
