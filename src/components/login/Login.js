import React from 'react';
import './Login.css';
import {Link, Redirect} from "react-router-dom";
import {connect} from "react-redux";
import {loginUser} from "../../store/actions/authActions";
import {storeUser} from "../../store/actions/userActions";

const Axios = require('axios').default;

class Login extends React.Component {
    state = {
        redirect: false,
        loading: false,
        email: '',
        password: '',
        error: '',
        emailError: ' ',
        passwordError: ' ',
        isFormValid: true
    }
    inputHandler = (event) => {
        const value = event.target.value
        switch (event.target.name) {
            case 'email':
                if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value)) {
                    this.setState({
                        email: value,
                        emailError: ''
                    })
                }else {
                    this.setState({
                        email: value,
                        emailError: 'Email is required and should be a valid email'
                    })
                }
                break;
            case 'password':
                if (value.length < 4) {
                    this.setState({
                        password: value,
                        passwordError: 'Password is required and should be at least 4 character long'
                    })
                }else {
                    this.setState({
                        password: value,
                        passwordError: ''
                    })
                }
                break;
            default:
                break;
        }
        if (this.state.emailError === '' && this.state.passwordError === '') {
            this.setState({
                isFormValid: true
            })
        }else {
            this.setState({
                isFormValid: false
            })
        }
    }
    formSubmit = (event) => {
        event.preventDefault();
        this.setState({
            loading: true
        }, () => {
            Axios.post('api/login', {
                email: this.state.email,
                password: this.state.password
            }, {
                headers: {
                    'Content-Type': 'application/json;charset=UTF-8',
                    "Access-Control-Allow-Origin": "*",
                    "Accept": "application/json"
                },
                referrerPolicy: 'no-referrer',
            }).then((res) => {
                localStorage.setItem('auth_token' , res.data.token);
                this.props.storeUser({
                    name: res.data.user.name,
                    email: res.data.user.email
                });
                this.props.loginUser();
                this.setState({
                    redirect: true,
                    loading: false,
                    error: ''
                })
            }).catch((err) => {
                this.setState({
                    loading: false,
                    error: err.response.data.msg
                })
            })

        })
    }
    render() {
        if (this.props.isLoggedIn) {
            return <Redirect to='/start'/>
        }
        return (
            <div className="main-login-wrapper">
                <form className="login-wrapper" onSubmit={this.formSubmit}>
                    <p>Login</p>
                    <input value={this.state.email} onChange={this.inputHandler} name="email" type="email" placeholder="Enter Email"/>
                    <p className="auth-error">{this.state.emailError}</p>
                    <input value={this.state.password} onChange={this.inputHandler} name="password" type="password" placeholder="Enter Password"/>
                    <p className="auth-error">{this.state.passwordError}</p>
                    <Link className="auth-switch" to="/signup">Dont have an account Signup?</Link>
                    <p className="password-error">{this.state.error}</p>
                    {
                        this.state.loading ? <div className="loader" style={{color: "#253237" , margin: '10px 20px 10px' , fontSize: '20px' , alignSelf: 'flex-end'}}></div> :
                            <button type="submit" className={this.state.isFormValid ? '' : 'disabled'}>
                                Login
                            </button>
                    }
                </form>
            </div>
        )
    }
}
const mapStateToProps = state => ({
    isLoggedIn: state.auth.isLoggedIn
})
export default connect(mapStateToProps, {loginUser,storeUser})(Login);
