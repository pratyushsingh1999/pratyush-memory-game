import React from 'react';
import './StartGame.css'
import { Link } from 'react-router-dom'
import { connect } from "react-redux";
import { updateGameDifficulty , updateGameMode } from "../../store/actions/gameActions";

class StartGame extends React.Component {

    constructor() {
        super();
        this.state = {
            difficulty: 'easy',
            gameMode: 'Color'
        }
        this.changeDifficulty = this.updateProperty('difficulty').bind(this);
        this.changeGameMode = this.updateProperty('gameMode').bind(this);
    }
    updateProperty(name) {
        return (event) => {
            this.setState({
                [name]: event.target.value,
            })
        }
    }
    changeDifficulty(event) {
        console.log("Hello world");
        this.props.updateGameDifficulty(event.target.value);
    }
    changeGameMode = (event) => {
        console.log("Hello world");
        this.props.updateGameMode(event.target.value);
    }
    render() {
        return (
            <div className="start-game-main">
                <div className="start-game-wrapper">
                    <div className="start-game-option-wrapper">
                        <p>Select Difficulty</p>
                        <select onChange={(event) => this.props.updateGameDifficulty(event.target.value)}
                                value={this.props.difficulty}>
                            <option value="easy">Easy</option>
                            <option value="medium">Medium</option>
                            <option value="hard">Hard</option>
                        </select>
                    </div>
                    <div className="start-game-option-wrapper">
                        <p>Select Game Mode</p>
                        <select onChange={(event) => this.props.updateGameMode(event.target.value)}
                                value={this.props.gameMode}>
                            <option value="Color">Colors</option>
                            <option value="Gif">Gifs</option>
                        </select>
                    </div>
                </div>
                <div className="start-game-wrapper button-wrapper">
                    <Link to="/playing">
                        <button>Start Game</button>
                    </Link>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    gameMode: state.game.gameMode,
    difficulty: state.game.difficulty,
})

export default connect(mapStateToProps, {updateGameDifficulty,updateGameMode})(StartGame);
