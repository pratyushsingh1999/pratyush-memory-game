import React from 'react';
import './GlobalScores.css';
import Axios from "axios";
import {Link} from "react-router-dom";
import throttler from "../../utils/throttler";

class GlobalScores extends React.Component {
    constructor() {
        super();
        this.state = {
            mode: 'easy',
            loading: true,
            list: []
        }
    }
    getEasyData = throttler(() => {
        this.setState({
            loading: true
        }, () => {
            Axios.get('api/getAllScores/easy' , {
                headers: {
                    'Content-Type': 'application/json;charset=UTF-8',
                    "Access-Control-Allow-Origin": "*",
                    "Accept": "application/json"
                },
                referrerPolicy: 'no-referrer',
            }).then((res) => {
                this.setState({
                    list: res.data,
                    loading: false
                })
            })
        })
    } , 500);
    getMediumData = throttler( () => {
        this.setState({
            loading: true
        }, () => {
            Axios.get('api/getAllScores/medium' , {
                headers: {
                    'Content-Type': 'application/json;charset=UTF-8',
                    "Access-Control-Allow-Origin": "*",
                    "Accept": "application/json"
                },
                referrerPolicy: 'no-referrer',
            }).then((res) => {
                this.setState({
                    list: res.data,
                    loading: false
                })
            })
        })
    } , 500)
    getHardData = throttler(() => {
        this.setState({
            loading: true
        }, () => {
            Axios.get('api/getAllScores/hard' , {
                headers: {
                    'Content-Type': 'application/json;charset=UTF-8',
                    "Access-Control-Allow-Origin": "*",
                    "Accept": "application/json"
                },
                referrerPolicy: 'no-referrer',
            }).then((res) => {
                this.setState({
                    list: res.data,
                    loading: false
                })
            })
        })
    }, 500);

    componentDidMount() {
        this.getEasyData();
    }

    render() {
        return (
            <div className="global-scores-wrapper">
                <Link to="/start">
                    <button className="go-back">Go Back</button>
                </Link>
                <p>Welcome to global scores</p>
                <div className="global-scores-nav">
                    <button onClick={this.getEasyData}>Easy</button>
                    <button onClick={this.getMediumData}>Medium</button>
                    <button onClick={this.getHardData}>Hard</button>
                </div>
                <table style={{display: 'flex' ,
                    alignItems: 'center' ,
                    flexDirection: 'column' ,
                    maxHeight: '80vh' ,
                    overflowY: 'scroll'}}>
                    <thead>
                        <tr>
                            <th>Rank</th>
                            <th>Player Name</th>
                            <th>Best Score</th>
                        </tr>
                    </thead>
                    <tbody >
                    {!this.state.loading ? this.state.list.map((ele , index) => {
                        return <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{ele.playerName}</td>
                            <td>{ele.bestScore}</td>
                        </tr>
                    }) : <div className="loader" style={{color: '#253237'}}></div>}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default GlobalScores;
