import React from 'react';
import {Route, Redirect} from 'react-router';
import {BrowserRouter as Router, Switch} from 'react-router-dom'
import './App.css';
import Axios from "axios";
import GameBoard from "./components/game-board/GameBoard";
import Login from "./components/login/Login";
import Signup from "./components/signup/Signup";
import StartGame from "./components/start-game/StartGame";
import GameOver from "./components/game-over/GameOver";
import GlobalScores from "./components/global-scores/GlobalScores";
import Header from "./components/header/Header";

import {connect} from 'react-redux';
import {loginUser} from "./store/actions/authActions";
import {storeUser} from "./store/actions/userActions";

Axios.defaults.baseURL = "https://backend-memory-game.herokuapp.com";

class App extends React.Component {
    constructor() {
        super();
        this.state = {
            loading: false
        }
    }

    componentDidMount() {
        this.setState({
            loading: true
        }, () => {
            if (!!localStorage.getItem("auth_token")) {
                Axios.get('api/getUserByToken', {
                    headers: {
                        authorization: 'bearer ' + localStorage.getItem("auth_token"),
                        'Content-Type': 'application/json;charset=UTF-8',
                        "Access-Control-Allow-Origin": "*",
                        "Accept": "application/json"
                    },
                    referrerPolicy: 'no-referrer'
                }).then((response) => {
                    const {name, email} = response.data.user;
                    this.props.storeUser({
                        name,
                        email
                    })
                })
                this.props.loginUser();
                this.setState({
                    loading: false
                })
            } else {
                this.setState({
                    loading: false
                })
            }
        })
    }

    render() {
        return (
            <Router>
                <Header></Header>
                {!this.state.loading ?
                    <Switch>
                        <Redirect exact from="/" to="/login"></Redirect>
                        <Route path="/login">
                            {!this.props.loggedIn ? <Login></Login> :
                                <Redirect to="/start"></Redirect>}
                        </Route>
                        <Route path="/signup">
                            <Signup></Signup>
                        </Route>
                        <Route path="/start">
                            {!this.props.loggedIn ? <Redirect to="/login"></Redirect> :
                                <StartGame></StartGame>}
                        </Route>
                        <Route path="/playing">
                            {!this.props.loggedIn ? <Redirect to="/login"></Redirect> :
                                <GameBoard></GameBoard>
                            }
                        </Route>
                        <Route path="/over">
                            {!this.props.loggedIn ? <Redirect to="/login"></Redirect> :
                                <GameOver></GameOver>}
                        </Route>
                        <Route path="/global-scores">
                            <GlobalScores></GlobalScores>
                        </Route>
                        <Route>
                            <div>
                                <p style={{color: "white", fontSize: "22px"}}>Error Page not found</p>
                            </div>
                        </Route>
                    </Switch> : <div className="loader"></div>}
            </Router>
        );
    }
}
const mapStateToProps = state => ({
    loggedIn: state.auth.loggedIn
})
export default connect(mapStateToProps, {loginUser,storeUser})(App);
