import { STORE_USER } from "./types";

export const storeUser = (payload) => (dispatch) => {
    dispatch({
        type: STORE_USER,
        payload
    })
}
