import { UPDATE_GAME_DIFFICULTY , UPDATE_GAME_MODE , UPDATE_GAME_SCORE } from "./types";

export const updateGameDifficulty = (payload) => (dispatch) => {
    console.log("from action");
    dispatch({
        type: UPDATE_GAME_DIFFICULTY,
        payload
    })
}

export const updateGameMode = (payload) => (dispatch) => {
    dispatch({
        type: UPDATE_GAME_MODE,
        payload
    })
}
export const updateGameScore = (payload) => (dispatch) => {
    dispatch({
        type: UPDATE_GAME_SCORE,
        payload
    })
}
