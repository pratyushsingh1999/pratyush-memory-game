import { UPDATE_GAME_SCORE , UPDATE_GAME_MODE , UPDATE_GAME_DIFFICULTY } from "../actions/types";

const initialState = {
    gameMode: 'Color',
    difficulty: 'easy',
    score: 0
}

const reducer = (state = initialState , action) => {
    switch (action.type) {
        case UPDATE_GAME_MODE:
            console.log("from game reducer" , action);
            return {
                ...state,
                gameMode: action.payload
            }
        case UPDATE_GAME_DIFFICULTY:
            return {
                ...state,
                difficulty: action.payload
            }
        case UPDATE_GAME_SCORE:
            return {
                ...state,
                score: action.payload
            }
        default: return state;
    }
}

export default reducer;
