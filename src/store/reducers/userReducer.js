import { STORE_USER } from "../actions/types";

const initialState = {
    username: '',
    email: ''
}

const reducer = (state = initialState , action) => {
    switch (action.type) {
        case STORE_USER:
            return {
                ...state,
                username: action.payload.name,
                email: action.payload.email
            };
        default:
            return {
                ...state
            };
    }
}

export default reducer;
