import { LOGIN_USER , LOGOUT_USER} from "../actions/types";

const initialState = {
    gameMode: 'Color',
    difficulty: 'easy',
    score: 0,
}

const reducer = (state = initialState , action) => {
    switch (action.type) {
        case LOGIN_USER:
            return {
                ...state,
                loggedIn: true
            }
        case LOGOUT_USER:
            return {
                ...state,
                loggedIn: false
            }
        default: return state;
    }
}

export default reducer;
