import { combineReducers } from "redux";
import authReducer from "./authReducer";
import userReducer from "./userReducer";
import gameReducer from "./gameReducer";
export default combineReducers({
    auth: authReducer,
    user: userReducer,
    game: gameReducer
})
