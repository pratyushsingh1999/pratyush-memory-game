import * as yup from 'yup';

const SignupSchema = yup.object({
    name: yup
        .string('Enter your name')
        .required('Name is required'),
    email: yup
        .string('Enter your email')
        .email('Enter a valid email')
        .required('Email is required'),
    password: yup
        .string('Enter your password')
        .min(4, 'Password should be of minimum 4 characters length')
        .required('Password is required'),
    passwordConfirm: yup.string()
        .oneOf([yup.ref('password'),'Passwords do not match'])
        .required('Password confirm is required')

})

export default SignupSchema;
